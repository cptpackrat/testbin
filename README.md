# @nfi/testbin
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

A set of utilities for creating mock binaries for testing.

## Installation
```
npm install @nfi/testbin
```

[npm-image]: https://img.shields.io/npm/v/@nfi/testbin.svg
[npm-url]: https://www.npmjs.com/package/@nfi/testbin
[pipeline-image]: https://gitlab.com/cptpackrat/testbin/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/testbin/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/testbin/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/testbin/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
