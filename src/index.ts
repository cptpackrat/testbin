import { makeTestDir } from '@nfi/testtree'
import { readFileSync, writeFileSync } from 'fs'

export interface TestBin {
  path: string
  read: () => string[][]
  wipe: () => void
  reset: () => void
}

export type TestAction = {
  type: 'exit'
  code: number
} | {
  type: 'sleep'
  delay: number
} | {
  type: 'stdout' | 'stderr'
  data: string
}

export function makeTestBin (actions?: TestAction[]): TestBin
export function makeTestBin (root: string, actions?: TestAction[]): TestBin
export function makeTestBin (root?: string | TestAction[], actions = root as TestAction[]): TestBin {
  const { join, wipe } = typeof root === 'string'
    ? makeTestDir(root)
    : makeTestDir()
  writeFileSync(join('bin.json'), '', { mode: '600' })
  writeFileSync(join('bin.js'), `#!${process.execPath}
    const { appendFile } = require('fs/promises')
    const actions = ${JSON.stringify(actions, null, 2)}
    ;(async () => {
      await appendFile('${join('bin.json')}', JSON.stringify(process.argv.slice(2)) + '\\n')
      for (const { type, code, data, delay } of actions) {
        switch (type) {
          case 'exit': process.exit(code)
          case 'stdout': process.stdout.write(data); break
          case 'stderr': process.stderr.write(data); break
          case 'sleep': await new Promise((resolve) => setTimeout(resolve, delay))
        }
      }
    })()
  `, { mode: '700' })
  return {
    path: join('bin.js'),
    wipe,
    reset: () => writeFileSync(join('bin.json'), '', { mode: '600' }),
    read: () => readFileSync(join('bin.json'), 'utf8')
      .split('\n')
      .slice(0, -1)
      .map((line) => JSON.parse(line))
  }
}
