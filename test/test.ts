import { makeTestBin } from '../src'
import { makeTestDir } from '@nfi/testtree'
import { existsSync, statSync } from 'fs'
import { execFile, execFileSync } from 'child_process'

describe('makeTestBin', () => {
  const { path, wipe } = makeTestDir()
  const one = makeTestBin()
  const two = makeTestBin(path)
  it('creates test binaries', () => {
    expect(statSync(one.path).isFile()).toBe(true)
    expect(statSync(two.path).isFile()).toBe(true)
    expect(two.path.startsWith(path)).toBe(true)
  })
  describe('read', () => {
    it('reads captured arguments', () => {
      execFileSync(two.path)
      execFileSync(two.path, [])
      execFileSync(two.path, ['foo', 'bar'])
      execFileSync(two.path, ['foo', 'bar', 'boo', 'baz'])
      expect(one.read()).toStrictEqual([])
      expect(two.read()).toStrictEqual([
        [],
        [],
        ['foo', 'bar'],
        ['foo', 'bar', 'boo', 'baz']
      ])
    })
  })
  describe('reset', () => {
    it('resets captured arguments', () => {
      one.reset()
      two.reset()
      expect(one.read()).toStrictEqual([])
      expect(two.read()).toStrictEqual([])
    })
  })
  describe('wipe', () => {
    it('deletes test binaries', () => {
      one.wipe()
      two.wipe()
      expect(existsSync(one.path)).toBe(false)
      expect(existsSync(two.path)).toBe(false)
    })
  })
  afterAll(() => {
    one.wipe()
    two.wipe()
    wipe()
  })
  describe('created binary', () => {
    const { path: root, wipe } = makeTestDir()
    it('performs "exit" action', async () => {
      const { path } = makeTestBin(root, [{ type: 'exit', code: 1 }])
      const { code } = await execTest(path)
      expect(code).toBe(1)
    })
    it('performs "sleep" action', async () => {
      const { path } = makeTestBin(root, [{ type: 'sleep', delay: 1000 }])
      const start = Date.now()
      await execTest(path)
      const delta = Date.now() - start
      expect(delta).toBeGreaterThan(975)
      expect(delta).toBeLessThan(1200)
    })
    it('performs "stdout" action', async () => {
      const { path } = makeTestBin(root, [{ type: 'stdout', data: 'test message' }])
      const { stdout } = await execTest(path)
      expect(stdout).toBe('test message')
    })
    it('performs "stderr" action', async () => {
      const { path } = makeTestBin(root, [{ type: 'stderr', data: 'test message' }])
      const { stderr } = await execTest(path)
      expect(stderr).toBe('test message')
    })
    it('performs combinations of actions', async () => {
      const { path } = makeTestBin(root, [
        { type: 'stdout', data: 'test message 1\n' },
        { type: 'sleep', delay: 250 },
        { type: 'stderr', data: 'test message 1\n' },
        { type: 'sleep', delay: 250 },
        { type: 'stdout', data: 'test message 2\n' },
        { type: 'sleep', delay: 250 },
        { type: 'stderr', data: 'test message 2\n' },
        { type: 'sleep', delay: 250 },
        { type: 'exit', code: 123 }
      ])
      const start = Date.now()
      const results = await execTest(path)
      const delta = Date.now() - start
      expect(delta).toBeGreaterThan(975)
      expect(delta).toBeLessThan(1200)
      expect(results).toStrictEqual({
        code: 123,
        stdout: 'test message 1\ntest message 2\n',
        stderr: 'test message 1\ntest message 2\n'
      })
    })
    afterAll(() => {
      wipe()
    })
  })
})

async function execTest (path: string): Promise<{
  code: number
  stdout: string
  stderr: string
}> {
  return await new Promise((resolve, reject) => {
    return execFile(path, (err: any, stdout, stderr) => {
      return err !== null
        ? typeof err.code === 'number'
          ? resolve({ code: err.code, stdout, stderr })
          : reject(err)
        : resolve({ code: 0, stdout, stderr })
    })
  })
}
